package mgl7460;

import static org.junit.Assert.*;

import org.junit.Test;

public class FileTest {
	@Test
	public void validatePathName() {
		File f = new File("a path");
		String exp = "base";
		assertEquals(exp, f.getBasename());
	}
}
